<?php

declare(strict_types=1);

namespace ZdenekGebauer\PasswordStrengthMeter;

readonly class Result
{
    /**
     * @param array<Violation> $violations
     */
    public function __construct(private array $violations)
    {
    }

    /**
     * @return array<Violation>
     */
    public function getViolations(): array
    {
        return $this->violations;
    }

    public function isStrong(): bool
    {
        return $this->violations === [];
    }
}
