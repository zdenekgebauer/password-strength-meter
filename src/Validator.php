<?php

declare(strict_types=1);

namespace ZdenekGebauer\PasswordStrengthMeter;

use function in_array;

readonly class Validator
{
    public function __construct(private Constraints $constraints)
    {
    }

    public function validate(string $password): Result
    {
        $length = mb_strlen($password);
        $containsNumber = (preg_match('/\d+/', $password) > 0);
        $containsUppercase = ($password !== mb_strtolower($password));
        $containsLowercase = ($password !== mb_strtoupper($password));
        $numberSpecialChars = 0;
        $containsRepeatedChars = false;
        $containsForbiddenText = false;

        $chars = mb_str_split($password);
        foreach ($chars as $char) {
            if (in_array($char, $this->constraints->getSpecialChars(), true)) {
                $numberSpecialChars++;
            }
        }

        for ($pos = 0; $pos < $length - 2; $pos++) {
            if ($password[$pos] === $password[$pos + 1] && $password[$pos] === $password[$pos + 2]) {
                $containsRepeatedChars = true;
                break;
            }
        }

        foreach ($this->constraints->getForbiddenTexts() as $forbiddenText) {
            if (stripos($password, $forbiddenText) !== false) {
                $containsForbiddenText = true;
                break;
            }
        }

        return new Result(
            array_filter(
                [
                    $length < $this->constraints->minLength ? Violation::TOO_SHORT : null,
                    $containsForbiddenText ? Violation::FORBIDDEN_TEXT : null,
                    $containsRepeatedChars ? Violation::REPEATED_CHARS : null,
                    !$containsNumber && $this->constraints->requiredNumber ? Violation::NO_NUMBER : null,
                    !$containsUppercase && $this->constraints->requiredUppercase ? Violation::NO_UPPER : null,
                    !$containsLowercase && $this->constraints->requiredLowercase ? Violation::NO_LOWER : null,
                    !$numberSpecialChars && $this->constraints->requiredSpecialChar ? Violation::NO_SPECIAL : null,
                ]
            )
        );
    }
}
