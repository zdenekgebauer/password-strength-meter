<?php

declare(strict_types=1);

namespace ZdenekGebauer\PasswordStrengthMeter;

enum Violation: string
{
    case TOO_SHORT = 'too-short';

    case  NO_NUMBER = 'no-number';

    case  NO_UPPER = 'no-upper-char';

    case  NO_LOWER = 'no-lower-char';

    case NO_SPECIAL = 'no-special-char';

    case  FORBIDDEN_TEXT = 'forbidden-text';

    case  REPEATED_CHARS = 'repeated-chars';
}
