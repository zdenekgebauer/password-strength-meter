<?php

declare(strict_types=1);

namespace ZdenekGebauer\PasswordStrengthMeter;

class Constraints
{
    public int $minLength = 4;

    public bool $requiredNumber = false;

    public bool $requiredUppercase = false;

    public bool $requiredLowercase = false;

    public bool $requiredSpecialChar = false;

    /**
     * @var array<string>
     */
    private array $specialChars;

    /**
     * @var array<string>
     */
    private array $forbiddenTexts = [
        '123',
        '456',
        '789',
        '987',
        '654',
        '321',
        'qwer',
        'zxcv',
        'yxcv',
        'admin',
        'pass',
        'heslo',
        'user',
        'login',
        'abc',
        'xyz',
    ];

    public function __construct()
    {
        $this->specialChars = mb_str_split(';!@#$%^&*()=+-_.,{}[]|<>?§');
    }

    /**
     * @param array<string> $texts
     */
    public function addForbiddenTexts(array $texts): void
    {
        $this->forbiddenTexts = array_merge($this->forbiddenTexts, array_map('\strval', $texts));
    }

    /**
     * @return array<string> case insensitive
     */
    public function getForbiddenTexts(): array
    {
        return $this->forbiddenTexts;
    }

    /**
     * @return array<string>
     */
    public function getSpecialChars(): array
    {
        return $this->specialChars;
    }

    public function setSpecialChars(string $specialChars): void
    {
        $this->specialChars = mb_str_split($specialChars);
    }
}
