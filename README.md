# Password Strength Meter 

Check password strength 

## Usage

Passwords can be validated by rules and/or by total score. 

### Check password by rules

```php
$constraints = new Constraints();
// optionally customize rules
$constraints->setMinLength(10);
$constraints->setRequiredNumber(true);
$constraints->setRequiredUppercase(true);
$constraints->setRequiredLowercase(true);
$constraints->setRequiredSpecialChar(true);

$result = (new Validator($constraints))->validate('aikfF56$');

if ($result->isStrong()) {
    echo 'password is OK', "\n";
} else {
    echo 'password is WEAK', "\n";
    echo 'violations:', implode("\n", $result->getViolations()), "\n";
}
```

### Check password by score

Score of password depends on length of password and contained characters:  
* longer than 4 chars: +1 point
* longer than 6 chars: +1 point
* longer than 8 chars: +1 point
* longer than 10 chars: +1 point
* contains number(s): +1 point
* contains uppercase letter(s): +1 point
* contains lowercase letter(s): +1 point
* contains special character(s): +1 point per special char
    
```php
$constraints = new Constraints();
$constraints->setMinScore(6);

$result = (new Validator($constraints))->validate('aikfF56$');

if ($result->isStrong()) {
    echo 'password is OK', "\n";
    echo 'score:', $result->getScore(), "\n";
} else {
    echo 'password is WEAK', "\n";
    echo 'score:', $result->getScore(), "\n";
}
```

### Forbidden texts

Default constraints forbids few texts used in most used passwords, like "password", "admin", "123", ...
 More texts can be added:
```php
$constraints = new Constraints();
$constraints->addForbiddenTexts(['heslo', 'iloveyou']);    
```
Default constraints also forbids more than two repeated characters in a row, like "aaa" or "some111".

