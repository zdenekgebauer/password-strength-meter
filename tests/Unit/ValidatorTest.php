<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\PasswordStrengthMeter\Constraints;
use ZdenekGebauer\PasswordStrengthMeter\Result;
use ZdenekGebauer\PasswordStrengthMeter\Validator;
use ZdenekGebauer\PasswordStrengthMeter\Violation;

class ValidatorTest extends Unit
{
    protected UnitTester $tester;

    public static function forbiddenTexts(): array
    {
        $passwords = [
            'Password',
            'p12345',
            'qwerty',
            '123456789',
            '987654321',
            'zxcvbnm',
            'yxcvvbnm',
            'admin',
            'password',
            'user',
            'login',
            'abcdef',
            'xyz',

        ];
        return array_combine($passwords, array_map(static fn(string $password) => [$password], $passwords));
    }

    public function testValidateByRules(): void
    {
        $constraints = new Constraints();
        $constraints->minLength = 6;
        $constraints->requiredNumber = true;
        $constraints->requiredUppercase  = true;
        $constraints->requiredLowercase = true;
        $constraints->requiredSpecialChar = true;
        $validator = new Validator($constraints);

        $result = $validator->validate('password');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertNotContains(Violation::TOO_SHORT, $result->getViolations());
        $this->tester->assertContains(Violation::NO_NUMBER, $result->getViolations());
        $this->tester->assertContains(Violation::NO_UPPER, $result->getViolations());
        $this->tester->assertNotContains(Violation::NO_LOWER, $result->getViolations());
        $this->tester->assertContains(Violation::NO_SPECIAL, $result->getViolations());
        $this->tester->assertContains(Violation::FORBIDDEN_TEXT, $result->getViolations());

        $result = $validator->validate('PAS2*');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::TOO_SHORT, $result->getViolations());
        $this->tester->assertNotContains(Violation::NO_NUMBER, $result->getViolations());
        $this->tester->assertNotContains(Violation::NO_UPPER, $result->getViolations());
        $this->tester->assertContains(Violation::NO_LOWER, $result->getViolations());
        $this->tester->assertNotContains(Violation::NO_SPECIAL, $result->getViolations());
    }

//    public function testValidateByScore(): void
//    {
//        $constraints = new Constraints();
//        $constraints->setMinScore(6);
//        $validator = new Validator($constraints);
//
//        $result = $validator->validate('glglqefgvbds');
//        $this->tester->assertFalse($result->isStrong());
//        $this->tester->assertEquals(5, $result->getScore());
//        $this->tester->assertContains(Result::TOO_WEAK, $result->getViolations());
//
//        $result = $validator->validate('AbQW12d*_');
//        $this->tester->assertTrue($result->isStrong());
//        $this->tester->assertEquals(8, $result->getScore());
//    }

    public function testValidateCustomForbiddenText(): void
    {
        $constraints = new Constraints();
        $constraints->addForbiddenTexts(['heslo']);
        $validator = new Validator($constraints);

        $result = $validator->validate('HESLO');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::FORBIDDEN_TEXT, $result->getViolations());
    }

    public function testValidateCustomSpecialChars(): void
    {
        $constraints = new Constraints();
        $constraints->requiredSpecialChar = true;
        $constraints->setSpecialChars('/');
        $validator = new Validator($constraints);

        $result = $validator->validate('Strong123');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::NO_SPECIAL, $result->getViolations());
    }

    /**
     * @dataProvider forbiddenTexts
     */
    public function testValidateForbiddenText(string $password): void
    {
        $constraints = new Constraints();
        $validator = new Validator($constraints);

        $result = $validator->validate($password);
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::FORBIDDEN_TEXT, $result->getViolations());
    }

    public function testValidateRepeatedChars(): void
    {
        $constraints = new Constraints();
        $validator = new Validator($constraints);

        $result = $validator->validate('some555');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::REPEATED_CHARS, $result->getViolations());
    }

    public function testValidateWithDefaultConstraints(): void
    {
        $constraints = new Constraints();

        $validator = new Validator($constraints);

        $result = $validator->validate('');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::TOO_SHORT, $result->getViolations());

        $result = $validator->validate('135');
        $this->tester->assertFalse($result->isStrong());
        $this->tester->assertContains(Violation::TOO_SHORT, $result->getViolations());

        $result = $validator->validate('1356');
        $this->tester->assertTrue($result->isStrong());
        $this->tester->assertNotContains(Violation::TOO_SHORT, $result->getViolations());
    }
}
